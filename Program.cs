﻿using System;

namespace tictactoe
{
    class Program
    {
        // Creating the playfiled
        private static char[,] playField =
        {
            {'1', '2', '3'},
            {'4', '5', '6'},
            {'7', '8', '9'}
        };

        static int turns = 0;

        static void Main(string[] args)
        {
            int player = 2; // Player 1 begins
            int input = 0;
            bool inputIsCorrect = true;



            // Runing code as long as the program runs
            do
            {
                if (player == 2)
                {
                    player = 1;
                    EnterXorO(player, input);
                }
                else if (player == 1)
                {
                    player = 2;
                    EnterXorO(player, input);
                }

                SetField();

                #region

                // Checking the winning conditions

                char[] playerChars = { 'X', 'O' };

                foreach (char playerChar in playerChars)
                {
                    if (((playField[0, 0] == playerChar) && (playField[0, 1] == playerChar) &&
                        (playField[0, 2] == playerChar))
                        || ((playField[1, 0] == playerChar) && (playField[1, 1] == playerChar) &&
                            (playField[1, 2] == playerChar))
                        || ((playField[2, 0] == playerChar) && (playField[2, 1] == playerChar) &&
                            (playField[2, 2] == playerChar))
                        || ((playField[0, 0] == playerChar) && (playField[1, 0] == playerChar) &&
                            (playField[2, 0] == playerChar))
                        || ((playField[0, 1] == playerChar) && (playField[1, 1] == playerChar) &&
                            (playField[2, 1] == playerChar))
                        || ((playField[0, 2] == playerChar) && (playField[1, 2] == playerChar) &&
                            (playField[2, 2] == playerChar))
                        || ((playField[0, 0] == playerChar) && (playField[1, 1] == playerChar) &&
                            (playField[2, 2] == playerChar))
                        || ((playField[0, 2] == playerChar) && (playField[1, 1] == playerChar) &&
                            (playField[2, 0] == playerChar))
                        )
                    {
                        if (playerChar == 'X')
                        {
                            Console.WriteLine("We have a winner!\nPlayer 2 has won!");
                        }
                        else
                        {
                            Console.WriteLine("We have a winner!\nPlayer 1 has won!");
                        }

                        Console.WriteLine("Please press any key to rest the game!");
                        Console.ReadKey();
                        ResetField();
                        break;
                    }
                    else if (turns == 10)
                    {
                        Console.WriteLine("\nDRAW!");
                        Console.WriteLine("Please press any key to rest the game!");
                        Console.ReadKey();
                        ResetField();
                        break;
                    }
                }

                #endregion

                #region
                // Testing if the field is already taken                
                do
                {
                    Console.WriteLine("\nPlayer {0}: Choose your field! ", player);
                    try
                    {
                        input = Convert.ToInt16(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Please enter a number!");
                    }

                    // trying to replace if statement with a switch statement
                    if ((input == 1) && (playField[0, 0] == '1'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 2) && (playField[0, 1] == '2'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 3) && (playField[0, 2] == '3'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 4) && (playField[1, 0] == '4'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 5) && (playField[1, 1] == '5'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 6) && (playField[1, 2] == '6'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 7) && (playField[2, 0] == '7'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 8) && (playField[2, 1] == '8'))
                    {
                        inputIsCorrect = true;
                    }
                    else if ((input == 9) && (playField[2, 2] == '9'))
                    {
                        inputIsCorrect = true;
                    }
                    else
                    {
                        Console.WriteLine("\nWrong input! Could you use another field, please!");
                        inputIsCorrect = false;
                    }


                } while (!inputIsCorrect);

                #endregion

            } while (true);
        }

        public static void ResetField()
        {
            char[,] playFieldInitial =
            {
                {'1', '2', '3'},
                {'4', '5', '6'},
                {'7', '8', '9'}
            };

            playField = playFieldInitial;
            SetField();
            turns = 0;
        }

        // setting the playfield
        public static void SetField()
        {
            Console.Clear();
            Console.WriteLine(" _____________________");
            Console.WriteLine("|      |       |      |");
            Console.WriteLine("|  {0}   |   {1}   |   {2}  |", playField[0, 0], playField[0, 1], playField[0, 2]);
            Console.WriteLine("|______|_______|______|");
            Console.WriteLine("|      |       |      |");
            Console.WriteLine("|  {0}   |   {1}   |   {2}  |", playField[1, 0], playField[1, 1], playField[1, 2]);
            Console.WriteLine("|______|_______|______|");
            Console.WriteLine("|      |       |      |");
            Console.WriteLine("|  {0}   |   {1}   |   {2}  |", playField[2, 0], playField[2, 1], playField[2, 2]);
            Console.WriteLine("|______|_______|______|");
            turns++;
        }

        // conditions
        public static void EnterXorO(int player, int input)
        {
            char playerSign = ' ';

            if (player == 1)
            {
                playerSign = 'X';
            }
            else if (player == 2)
            {
                playerSign = 'O';
            }

            switch (input)
            {
                case 1: playField[0, 0] = playerSign; break;
                case 2: playField[0, 1] = playerSign; break;
                case 3: playField[0, 2] = playerSign; break;
                case 4: playField[1, 0] = playerSign; break;
                case 5: playField[1, 1] = playerSign; break;
                case 6: playField[1, 2] = playerSign; break;
                case 7: playField[2, 0] = playerSign; break;
                case 8: playField[2, 1] = playerSign; break;
                case 9: playField[2, 2] = playerSign; break;
            }
        }
    }
}